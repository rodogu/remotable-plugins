package com.atlassian.plugin.remotable.sisu;

public final class DisposeException extends Throwable
{
    public DisposeException(Throwable throwable)
    {
        super(throwable);
    }
}
