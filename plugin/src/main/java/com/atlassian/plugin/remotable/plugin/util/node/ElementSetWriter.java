package com.atlassian.plugin.remotable.plugin.util.node;

/**
 */
public interface ElementSetWriter extends ElementWriter<ElementSetWriter>
{
    SingleElementWriter done();
}
